package com.mimpi.sab143040106.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mimpi.sab143040106.R;
import com.mimpi.sab143040106.Task1Activity;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tab1Fragment extends Fragment {

    private EditText edtPanjang, edtLebar;
    private Button btnHitung;
    private TextView txtLuas;


    public Tab1Fragment() {
        // Required empty public constructor

    }

    public static Task1Activity task1Activity;

    public static Tab1Fragment newInstance (Task1Activity activity){
        task1Activity = activity;
        return new Tab1Fragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_tab1, container, false);
        View v = inflater.inflate(R.layout.fragment_tab1, container, false);

        edtPanjang = (EditText)v.findViewById(R.id.edt_panjang);
        edtLebar = (EditText)v.findViewById(R.id.edt_lebar);
        btnHitung = (Button)v.findViewById(R.id.btn_hitung);
        txtLuas = (TextView)v.findViewById(R.id.txt_luas);

        btnHitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String panjang = edtPanjang.getText().toString().trim();
                String lebar = edtLebar.getText().toString().trim();

                double p = Double.parseDouble(panjang);
                double l = Double.parseDouble(lebar);
                if(p == 0){
                    edtPanjang.setError("Panjang Tidak Boleh Kosong");
                    edtPanjang.requestFocus();
                } else if (l == 0){
                    edtLebar.setError("Lebar Tidak Boleh Kosong");
                    edtLebar.requestFocus();
                } else {
                    double luas = p * l;

                    txtLuas.setText("Luas : "+luas);
                }
            }
        });
        return v;


    }

}
